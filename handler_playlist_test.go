package quaggamusic

import (
	"bytes"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestPlaylistHandler(t *testing.T) {

	var testBody = "{\"Artists\" : [\"Sabaton\"]}"
	var jsonStr = []byte(testBody)

	req, err := http.NewRequest("GET", "/api/v1/playlist", bytes.NewBuffer(jsonStr))
	if err != nil {
		log.Print(err)
	}

	rw := httptest.NewRecorder()
	handler := http.HandlerFunc(HandlerPlaylist)

	handler.ServeHTTP(rw, req)

	if status := rw.Code; status != http.StatusOK {
		t.Errorf("Got non 200 status code: %d", status)
	}
}
