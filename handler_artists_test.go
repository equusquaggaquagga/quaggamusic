package quaggamusic

import (
	"bytes"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHandlerArtist(t *testing.T) {

	body := "{\"song\" : \"ocean_man\"}"
	var jsonStr = []byte(body)

	req, err := http.NewRequest("GET", "/api/v1/searchArtists", bytes.NewBuffer(jsonStr))
	if err != nil {
		log.Print(err)
	}

	rw := httptest.NewRecorder()
	handler := http.HandlerFunc(HandlerArtists)

	handler.ServeHTTP(rw, req)

	if status := rw.Code; status != http.StatusOK {
		t.Errorf("Got non 200 status code: %d", status)
	}
}

func TestHandlerArtistWithBrokenBody(t *testing.T) {

	body := "{\"broken payload\"}"
	var jsonStr = []byte(body)

	req, err := http.NewRequest("GET", "/api/v1/searchArtists", bytes.NewBuffer(jsonStr))
	if err != nil {
		log.Print(err)
	}

	rw := httptest.NewRecorder()
	handler := http.HandlerFunc(HandlerArtists)

	handler.ServeHTTP(rw, req)

	if status := rw.Code; status != http.StatusBadRequest {
		t.Errorf("Got non 400 status code: %d", status)
	}
}

func TestHandlerArtistWithNoSong(t *testing.T) {

	body := "{\"song\" : \"\"}"
	var jsonStr = []byte(body)

	req, err := http.NewRequest("GET", "/api/v1/searchArtists", bytes.NewBuffer(jsonStr))
	if err != nil {
		log.Print(err)
	}

	rw := httptest.NewRecorder()
	handler := http.HandlerFunc(HandlerArtists)

	handler.ServeHTTP(rw, req)

	if status := rw.Code; status != http.StatusBadRequest {
		t.Errorf("Got non 400 status code: %d", status)
	}
}

func TestHandlerArtistWrongSongAndYear(t *testing.T) {

	body := "{\"song\" : \"something\", \"year\" : \"2019\"}"
	var jsonStr = []byte(body)

	req, err := http.NewRequest("GET", "/api/v1/searchArtists", bytes.NewBuffer(jsonStr))
	if err != nil {
		log.Print(err)
	}

	rw := httptest.NewRecorder()
	handler := http.HandlerFunc(HandlerArtists)

	handler.ServeHTTP(rw, req)

	if status := rw.Code; status != http.StatusNotFound {
		t.Errorf("Got non 404 status code: %d", status)
	}
}

func TestHandlerArtistWrongSongAndGenre(t *testing.T) {

	body := "{\"song\" : \"something\", \"genre\" : \"non-existent one\"}"
	var jsonStr = []byte(body)

	req, err := http.NewRequest("GET", "/api/v1/searchArtists", bytes.NewBuffer(jsonStr))
	if err != nil {
		log.Print(err)
	}

	rw := httptest.NewRecorder()
	handler := http.HandlerFunc(HandlerArtists)

	handler.ServeHTTP(rw, req)

	if status := rw.Code; status != http.StatusNotFound {
		t.Errorf("Got non 404 status code: %d", status)
	}
}

func TestHandlerArtistWrongSong(t *testing.T) {

	body := "{\"song\" : \"something\"}"
	var jsonStr = []byte(body)

	req, err := http.NewRequest("GET", "/api/v1/searchArtists", bytes.NewBuffer(jsonStr))
	if err != nil {
		log.Print(err)
	}

	rw := httptest.NewRecorder()
	handler := http.HandlerFunc(HandlerArtists)

	handler.ServeHTTP(rw, req)

	if status := rw.Code; status != http.StatusNotFound {
		t.Errorf("Got non 404 status code: %d", status)
	}
}

func TestFilter(t *testing.T) {
	var test releaseTitleResults
	data := discogsResults{"Somesong", "1969", []string{"Rock", "ProgRock"}}
	test.Results = append(test.Results, data)

	testRes := filterYear(&test, "1969")
	if testRes.Results[0].Title != "Somesong" {
		t.Errorf("ERROR: The function does not correctly filter by year!")
	}

	testRes = filterGenre(&test, "Rock")
	if testRes.Results[0].Title != "Somesong" {
		t.Errorf("ERROR: The function does not correctly filter by genre!")
	}
}

func TestReplaceSpace(t *testing.T) {
	test := []string{
		"This",
		"is",
		"a",
		"string"}

	if "This+is+a+string" != replaceSpace(test) {
		t.Errorf("ERROR: The function does not correctly replace speces!")
	}

	test2 := []string{"test"}

	if "test" != replaceSpace(test2) {
		t.Errorf("ERROR: The function does not correctly preserve words without spaces!")
	}
}
