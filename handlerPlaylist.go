package quaggamusic

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	firebase "firebase.google.com/go"
	"google.golang.org/api/option"
)

//HandlerPlaylist handles playlist endpoint
func HandlerPlaylist(w http.ResponseWriter, r *http.Request) {

	var playlistMessage []responsePlaylist
	var artistsPayload artistsPayload

	limit := r.FormValue("limit")

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Print(err)
	}

	err = json.Unmarshal(body, &artistsPayload)
	if err != nil {
		log.Print(err)
	}

	if len(artistsPayload.Artists) < 1 {
		errorWrapper(w, "Body invalid. Make sure it's not empty and written correctly.", http.StatusBadRequest)
		return
	}

	playlistMessage, err = getPlaylist(artistsPayload, limit)
	if err != nil {
		log.Print(err)
		return
	}

	var buffer = new(bytes.Buffer)

	//	Sending generated playlist to database
	var allArtists string
	var allSongTitles string
	var allVideoURLs string

	allArtists = playlistMessage[0].ArtistName
	allSongTitles = playlistMessage[0].SongName
	allVideoURLs = playlistMessage[0].URL

	for i := 1; i < len(playlistMessage); i++ {

		allArtists = allArtists + "|" + playlistMessage[i].ArtistName
		allSongTitles = allSongTitles + "|" + playlistMessage[i].SongName
		allVideoURLs = allVideoURLs + "|" + playlistMessage[i].URL

	}

	sendToDatabase(allArtists, allSongTitles, allVideoURLs)

	w.Header().Add("Content-Type", "application/json")
	err = json.NewEncoder(buffer).Encode(playlistMessage)

	if err != nil {
		errorWrapper(w, "Couldn't encode the file", http.StatusInternalServerError)
	}

	_, err = io.Copy(w, buffer)
	if err != nil {
		log.Print(err)
	}

}

func sendToDatabase(allArtists string, allSongTitles string, allURLs string) {

	var payload databasePayload

	// Creating a Firebase connection
	ctx := context.Background()
	sa := option.WithCredentialsFile("firebase.json")
	app, err := firebase.NewApp(ctx, nil, sa)
	if err != nil {
		log.Print(err)
	}

	client, err := app.Firestore(ctx)
	if err != nil {
		log.Print(err)
	}
	defer client.Close()

	payload.SongArtists = allArtists
	payload.SongTitles = allSongTitles
	payload.VideoURLs = allURLs

	_, _, err = client.Collection("playlists").Add(ctx, payload)
	if err != nil {
		log.Print(err)
	}

}

func getPlaylist(artistsPayload artistsPayload, limit string) ([]responsePlaylist, error) {

	var playlistMessage []responsePlaylist

	//Goes through every parsed artist in body
	//and gets the most popular songs from lastFM
	for i := 0; i < len(artistsPayload.Artists); i++ {
		var topTracks topTracksFM

		lastFMAddress := fMURL + "&artist=" + artistsPayload.Artists[i] //PLZ FIX

		req, err := http.Get(lastFMAddress)
		if err != nil {
			log.Print(err)
		}

		err = json.NewDecoder(req.Body).Decode(&topTracks)
		if err != nil {
			log.Print(err)
		}

		for j := 0; j < len(topTracks.TopTracks.Tracks); j++ {
			var tempPlaylistMessage responsePlaylist

			tempPlaylistMessage.ArtistName = artistsPayload.Artists[i]
			tempPlaylistMessage.SongName = topTracks.TopTracks.Tracks[j].Name
			tempPlaylistMessage.URL = topTracks.TopTracks.Tracks[j].URL

			playlistMessage = append(playlistMessage, tempPlaylistMessage)

		}

	}

	if len(playlistMessage) < 1 {
		log.Println("Invalid band name or it doesn't exist. Try using different band name.")
		return playlistMessage, errors.New("Invalid band name or it doesn't exist")
	}
	//Randomizes the results to keep the playlist spicy
	seed := time.Now().Unix() * time.Now().Unix()
	rand.Seed(seed)

	for i := range playlistMessage {
		j := rand.Intn(i + 1)
		playlistMessage[i], playlistMessage[j] = playlistMessage[j], playlistMessage[i]
	}

	//	Limit the amount of results
	if limit != "" {
		limitInt, _ := strconv.Atoi(limit)
		if limitInt > len(playlistMessage) {
			playlistMessage = playlistMessage[len(playlistMessage):]
		} else if limitInt == 0 {
			log.Println("Invalid band name or it doesn't exist. Try using different band name.")
			return playlistMessage, errors.New("Limit can't be 0")
		} else {
			playlistMessage = playlistMessage[:limitInt]
		}
	} else {
		playlistMessage = playlistMessage[:10]
	}

	return playlistMessage, nil
}
