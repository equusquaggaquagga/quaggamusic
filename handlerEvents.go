package quaggamusic

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"net/url"
)

//HandlerEvents handles searchEvent endpoint.
func HandlerEvents(w http.ResponseWriter, r *http.Request) {
	var response response

	keyword := r.FormValue("keyword")
	startDate := r.FormValue("startDate")
	endDate := r.FormValue("endDate")
	country := r.FormValue("country")
	city := r.FormValue("city")
	page := r.FormValue("page")

	address := eventURL

	//Necessary input to make TicketMaster API work.
	if startDate != "" {
		startDate = startDate + "T00:00:00Z"
	}

	if endDate != "" {
		endDate = endDate + "T00:00:00Z"
	}

	//Checks if one of three key requirements is used.
	if keyword == "" && city == "" && country == "" {
		errorWrapper(w, "You at least keyword/City/Country to use this endpoint!", http.StatusBadRequest)
		return
	}

	//  Adding existing query parameters
	address = addParam("keyword", keyword, address)
	address = addParam("startDateTime", startDate, address)
	address = addParam("endDateTime", endDate, address)
	address = addParam("countryCode", country, address)
	address = addParam("city", city, address)
	address = addParam("page", page, address)

	response, err := getResponse(address)

	if err != nil {
		errorWrapper(w, "404 Not Found", http.StatusNotFound)
		return
	}

	var buffer = new(bytes.Buffer)

	err = json.NewEncoder(buffer).Encode(response)
	if err != nil {
		errorWrapper(w, "Could not encode the struct!", http.StatusInternalServerError)
	}

	w.Header().Add("Content-Type", "application/json") // Displays the mesage in json formating
	w.WriteHeader(http.StatusOK)

	//Streams binary into ResponseWriter

	_, err = io.Copy(w, buffer)
	if err != nil {
		log.Print(err)
	}
}

func getResponse(address string) (response, error) {

	var tempResponse response
	var message eventsReceived

	var eventResponseArray []eventResponse

	res, err := http.Get(address)

	if err != nil {
		log.Print(err)
		return tempResponse, err
	}

	defer res.Body.Close()

	err = json.NewDecoder(res.Body).Decode(&message)

	//Checks for errors
	if err != nil {
		log.Print(err)
		return tempResponse, err
	}

	//Goes through every event
	for i := 0; i < len(message.Embedded.Events); i++ {
		var temp eventResponse

		//Insert all data to a temp which will be later inserted into main struct
		temp.Name = message.Embedded.Events[i].Name

		temp.EventDateTime = message.Embedded.Events[i].Dates.StartDate.LocalDate + "T" + message.Embedded.Events[i].Dates.StartDate.LocalTime

		temp.Tickets.SaleStart = message.Embedded.Events[i].Sales.SalesPublic.StartDateTime
		temp.Tickets.SaleEnd = message.Embedded.Events[i].Sales.SalesPublic.EndDateTime
		temp.Tickets.Status = message.Embedded.Events[i].Dates.Status.Code

		temp.Link = message.Embedded.Events[i].URL

		//Goes through all venues for a single event.
		//This is necessary since a single events can have multiple venues.
		for j := 0; j < len(message.Embedded.Events[i].InnerEmbedded.Venues); j++ {

			var tempVenue venueResponse

			tempVenue.Address = message.Embedded.Events[i].InnerEmbedded.Venues[j].Address.Line
			tempVenue.City = message.Embedded.Events[i].InnerEmbedded.Venues[j].City.Name
			tempVenue.State = message.Embedded.Events[i].InnerEmbedded.Venues[j].State.Name
			tempVenue.Country = message.Embedded.Events[i].InnerEmbedded.Venues[j].Country.Name

			temp.VenuesResponse = append(temp.VenuesResponse, tempVenue)

		}

		eventResponseArray = append(eventResponseArray, temp)
	}

	tempResponse.TotalPages = message.Page.TotalPages
	tempResponse.EventResponse = eventResponseArray

	return tempResponse, nil
}

//Handles adding necessary things into URL.
func addParam(paramName string, paramValue string, URL string) string {

	if paramValue != "" {
		URL += paramName + "=" + url.QueryEscape(paramValue) + "&"
	}

	return URL
}
