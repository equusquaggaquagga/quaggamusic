package quaggamusic

const searchForSongName = "https://api.discogs.com/database/search?release_title="
const discogsAuth = "qwbjjPdATBvRjHixpiTMRCtgGwGzRbCwWTwFzbeo"
const youtubeSearchURL = "https://www.youtube.com/results?search_query="

type payloadArtists struct {
	Song  string `json:"song"`
	Year  string `json:"year"`
	Genre string `json:"genre"`
}

type urls struct {
	Last string `json:"last"`
	Next string `json:"next"`
}

type pagination struct {
	PerPage int  `json:"per_page"`
	Pages   int  `json:"pages"`
	Page    int  `json:"page"`
	Urls    urls `json:"urls"`
}

type discogsResults struct {
	Title string   `json:"title"`
	Year  string   `json:"year"`
	Genre []string `json:"genre"`
}

type releaseTitleResults struct {
	Pagination pagination       `json:"pagination"`
	Results    []discogsResults `json:"results"`
}

type artistDelivStruct struct {
	Searchstring string       `json:"searchstring"`
	ArtistData   []artistData `json:"songs"`
}

type artistData struct {
	SongName string   `json:"songname"`
	BandName string   `json:"bandname"`
	Year     string   `json:"year"`
	Genre    []string `json:"genre"`
	Link     string   `json:"youtubeurl"`
}
