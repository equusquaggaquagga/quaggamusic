### Quaggamusic


## Usage
All the information about usage of our API is found on the root site.

## Project Idea
When designing the API, we wanted to make it easy for users to find information about specific songs and artists, as well as their favourite live music events.
Then we designed various endpoints with regard to that:

  I. The /searchArtists endpoint can be used to find artists who played a particular song. This can be used to the original author, to find all the various covers, and others.

  II. The /searchEvents endpoint can be used to find live music events. The user can specify keyword, city and country to narrow down the results.

  III. The /searchBands enpoint can be used to retrieve full information about bands by name. The user specifies the name, and the server returns all bands with that name, as well as their albums and tracks.

  IV. The /playlist returns a random playlist consisting of songs from the bands that were specified by the user. It can be used for many purposes: discovering new music, creating random playlist for various events, and so on. Every created playlist is added to the database and assigned and ID, so that it can be accessed later.

  V. The /playlistDatabase endpoint connects to the playlists database and can display either all the registered playlists, or a specific one.


## What went well
In the planning phase of the project we decided that the more efficient strategy would be doing pair programming in groups of two, while meeting regularly to keep everyone updated and make changes to the plan and schedule. This allowed us to still focus on many different tasks at the same time, while also having a partner for more efficiency and proof-reading. We also were quite successful in planning the scope of the project. Most of the planned endpoints were implemented, and the only one that was discarded was discarded because it was unnecessary, and not due to time or resource limitations. Instead, one unplanned endpoint was implemented.

## What went bad
While working on the project, we wanted to use the more popular services to hopefully get more precise data. Youtube was one of these large services we wanted to get some data from, in this instance we wanted to get direct links to different songs that the user would be searching for. While we were experimenting with the API, we swiftly noticed that the quota limit for Youtube was not enough and that there were no other convenient way to get the ID of a video, so we decided to switch over to lastFM songs. We also tried to utilize Spotify API to for example get the most popular songs from an artist, but the process of obtaining the API key was harder than we anticipated. We believe using Spotify API would give us more engaging results and were disappointed we couldn't use it.
To make the API more interesting and useful, plans were made to create some sort of playlist randomizer. In the end we came up with the idea of two playlists. First one was supposed to return most popular songs of a band together with a Youtube link redirecting directly to a Youtube song. The second playlist endpoint was supposed to receive several artists and return random songs from all of the parsed artists. The most popular songs endpoint was later discarded, because it basically does the same as randomizer endpoint. Many artists had few or even no songs which would make this API function not very useable from collecting data. We also looked through multiple other musical APIs with no luck in finding functionality that would let us find the songs of a specific artist ranked by popularity. In addition, as there were two playlist endpoint we in the end decided to drop the first one so we could focus on other tasks.
Other than that, we have managed to realize most of our plans and we are happy with the result.

## What we have learned
One of the most important things that we learned during this project was the actual proccess of developing a medium-sized API on our own, that includes using multiple other APIs, a database, a cloud server and a front-end interface, from bottom to top, in a limited amount of time.
During this project, we were exposed to several different API documentations. Some were good, some were not so much. We have learned how to effectively navigate different API documentations. We also discovered that some API's are strictly suited to one use and becomes hard to utilize once you try to use them for other purposes because of their policy.
We have also learned how to use Discord bots and how to implement them into our own application.

## Time spent in total
101 hours in total

## Team members
* Artūrs Umbraško (artursu)
* Kacper Lewandowski (kacperl)
* Kristoffer Madsen (krismad)
* Daniel Dahl (daniedah)
