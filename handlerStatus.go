package quaggamusic

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"time"
)

//Uptime is the amount of time the program was running
var Uptime time.Time

//HandlerStatus handles status endpoint
func HandlerStatus(w http.ResponseWriter, r *http.Request) {

	var statusResponse statusResponse

	transport := &http.Transport{

		TLSClientConfig: &tls.Config{
			Renegotiation:      tls.RenegotiateFreelyAsClient,
			InsecureSkipVerify: true},
	}

	client := &http.Client{

		Transport: transport,
	}

	statusResponse.Version = "V1"

	address := "https://api.discogs.com"

	res, err := http.Get(address)
	if err != nil {
		log.Print(err)
	}
	defer res.Body.Close()

	statusResponse.Discogs = res.StatusCode

	var buffer = new(bytes.Buffer)

	address = "http://ws.audioscrobbler.com"

	res, err = http.Get(address)
	if err != nil {
		log.Print(err)
	}
	defer res.Body.Close()

	statusResponse.LastFM = res.StatusCode

	address = "https://app.ticketmaster.com/discovery/v2/events.json?segmentId=KZFzniwnSyZfZ7v7nJ&apikey=WK7041MZi6mNjYvqn3aemHLPsujlKzz1"

	res, err = http.Get(address)
	if err != nil {
		log.Print(err)
	}
	defer res.Body.Close()

	statusResponse.TicketMaster = res.StatusCode

	address = "https://theaudiodb.com"

	req, _ := http.NewRequest("GET", address, nil)
	res, err = client.Do(req)

	if err != nil {

		log.Print(err)
	}

	defer res.Body.Close()

	statusResponse.AudioDB = res.StatusCode
	statusResponse.Uptime = time.Since(Uptime).String()

	err = json.NewEncoder(buffer).Encode(statusResponse)
	if err != nil {
		errorWrapper(w, "Couldn't encode the file!", http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json") // Displays the mesage in json formating

	_, err = io.Copy(w, buffer)
	if err != nil {
		log.Print(err)
	}

}

//	Returns error message
func errorWrapper(w http.ResponseWriter, message string, status int){

	var chuck chucknorris

	res, err := http.Get("https://api.chucknorris.io/jokes/random")

	if err != nil {

		log.Print(err)
		return

	}

	defer res.Body.Close()

	err = json.NewDecoder(res.Body).Decode(&chuck)

	if err != nil {

		log.Print(err)
		return

	}


	http.Error(w, message + "\n\n\n\n\nDid you know?\n" + chuck.Value, status)

}
