package quaggamusic

type statusResponse struct {
	Version      string `json:"version"`
	Uptime       string `json:"Uptime"`
	Discogs      int    `json:"Discogs"`
	LastFM       int    `json:"LastFM"`
	TicketMaster int    `json:"TicketMaster"`
	AudioDB      int    `json:"AudioDB"`
}

type chucknorris struct {

	Value string `json:"value"`

}
