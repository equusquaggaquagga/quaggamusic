package quaggamusic

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"strings"

	firebase "firebase.google.com/go"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
)

//HandlerPlaylistDatabase handles database
func HandlerPlaylistDatabase(w http.ResponseWriter, r *http.Request) {

	var songArtists []string
	var songTitles []string
	var songURLs []string

	ID := r.FormValue("id")

	if ID == "" {

		//  Requesting playlist from database

		var response []responsePlaylistWithID

		playlistsList := getDatabase()

		//  Splitting it into songs
		for i := 0; i < len(playlistsList); i++ {

			var tempResponse responsePlaylistWithID

			songArtists = strings.Split(playlistsList[i].SongArtists, "|")
			songTitles = strings.Split(playlistsList[i].SongTitles, "|")
			songURLs = strings.Split(playlistsList[i].VideoURLs, "|")

			//  Assigning songs to separate objects
			for j := 0; j < len(songArtists); j++ {

				var temp responsePlaylist

				temp.SongName = songTitles[j]
				temp.ArtistName = songArtists[j]
				temp.URL = songURLs[j]

				tempResponse.ResponsePlaylist = append(tempResponse.ResponsePlaylist, temp)
			}

			tempResponse.ID = playlistsList[i].ID

			response = append(response, tempResponse)
		}

		var buffer = new(bytes.Buffer)

		err := json.NewEncoder(buffer).Encode(response)
		if err != nil {
			errorWrapper(w, "Could not encode the struct!", http.StatusInternalServerError)
		}

		w.Header().Add("Content-Type", "application/json") // Displays the mesage in json formating
		w.WriteHeader(http.StatusOK)

		//Streams binary into ResponseWriter

		_, err = io.Copy(w, buffer)
		if err != nil {
			log.Print(err)
		}

	} else {

		var response responsePlaylistWithID

		playlist := getSpecificPlaylist(w, ID)

		songArtists = strings.Split(playlist.SongArtists, "|")
		songTitles = strings.Split(playlist.SongTitles, "|")
		songURLs = strings.Split(playlist.VideoURLs, "|")

		//  Assigning songs to separate objects
		for j := 0; j < len(songArtists); j++ {

			var temp responsePlaylist

			temp.SongName = songTitles[j]
			temp.ArtistName = songArtists[j]
			temp.URL = songURLs[j]

			response.ResponsePlaylist = append(response.ResponsePlaylist, temp)
		}

		response.ID = playlist.ID

		var buffer = new(bytes.Buffer)

		err := json.NewEncoder(buffer).Encode(response)
		if err != nil {
			errorWrapper(w, "Could not encode the struct!", http.StatusInternalServerError)
		}

		w.Header().Add("Content-Type", "application/json") // Displays the mesage in json formating
		w.WriteHeader(http.StatusOK)

		//Streams binary into ResponseWriter

		_, err = io.Copy(w, buffer)
		if err != nil {
			log.Print(err)
		}

	}

}


//	Every playlist from database
func getDatabase() []databasePlaylist {

	var playlistsList []databasePlaylist

	// Creating a Firebase connection
	ctx := context.Background()
	sa := option.WithCredentialsFile("firebase.json")
	app, err := firebase.NewApp(ctx, nil, sa)
	if err != nil {
		log.Print(err)
	}

	client, err := app.Firestore(ctx)
	if err != nil {
		log.Print(err)
	}
	defer client.Close()

	iter := client.Collection("playlists").Documents(ctx)

	for {
		var current databasePlaylist

		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Print(err)
		}

		err = doc.DataTo(&current)
		if err != nil {
			log.Print(err)
		}
		current.ID = doc.Ref.ID

		playlistsList = append(playlistsList, current)

	}

	return playlistsList

}

//	Gets specific playlist from database
func getSpecificPlaylist(w http.ResponseWriter, ID string) databasePlaylist {

	var temp databasePlaylist

	ctx := context.Background()
	sa := option.WithCredentialsFile("firebase.json")
	app, err := firebase.NewApp(ctx, nil, sa)
	if err != nil {
		log.Print(err)
	}

	client, err := app.Firestore(ctx)
	if err != nil {
		log.Print(err)
	}
	defer client.Close()

	iter := client.Collection("playlists").Documents(ctx)
	for {

		current, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Print(err)
		}

		if current.Ref.ID == ID {

			err = current.DataTo(&temp)
			if err != nil {
				log.Print(err)
			}
			temp.ID = current.Ref.ID

			return temp

		}

	}

	errorWrapper(w, "404 Playlist not found!", http.StatusNotFound)
	return temp

}
