package quaggamusic

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"sort"
	"strconv"
)

//HandlerBands handles searchBands endpoint
func HandlerBands(w http.ResponseWriter, r *http.Request) {

	var artists receivedArtists
	var messageResponse []artistResponse

	var tempArtist artistResponse
	var tempTrack trackResponse

	var albums receivedAlbums
	var tracks receivedTracks

	//  This is stolen from StackOverflow, I don't know what most of this means
	//  I wish I knew what TLS is
	transport := &http.Transport{

		TLSClientConfig: &tls.Config{
			Renegotiation:      tls.RenegotiateFreelyAsClient,
			InsecureSkipVerify: true},
	}

	client := &http.Client{

		Transport: transport,
	}
	//  End of the stolen part

	address := bandsURL + "/search.php?"
	name := r.FormValue("name")

	if name == "" {
		errorWrapper(w, "Invalid name, you need to insert name of a band!", http.StatusBadRequest)
		return
	}

	address = addParam("s", name, address)

	req, _ := http.NewRequest("GET", address, nil)
	res, err := client.Do(req)

	if err != nil {
		log.Print(err)
	}

	err = json.NewDecoder(res.Body).Decode(&artists)

	if err != nil {
		log.Print(err)
	}

	defer res.Body.Close()

	//  Close your eyes

	//Requesting all albums from an artist
	for i := 0; i < len(artists.Artists); i++ {

		tempArtist.StrArtist = artists.Artists[i].StrArtist
		tempArtist.IntFormedYear = artists.Artists[i].IntFormedYear
		tempArtist.IntBornYear = artists.Artists[i].IntBornYear
		tempArtist.IntDiedYear = artists.Artists[i].IntDiedYear
		if artists.Artists[i].StrDisbanded == "Yes" {

			tempArtist.BooDisbanded = true

		} else {

			tempArtist.BooDisbanded = false

		}
		tempArtist.StrStyle = artists.Artists[i].StrStyle
		tempArtist.StrGenre = artists.Artists[i].StrGenre
		tempArtist.StrMood = artists.Artists[i].StrMood
		tempArtist.StrBiographyEN = artists.Artists[i].StrBiographyEN
		tempArtist.IntMembers = artists.Artists[i].IntMembers

		messageResponse = append(messageResponse, tempArtist)

		address = bandsURL + "/album.php?"
		address = addParam("i", artists.Artists[i].IDArtist, address)

		req, _ = http.NewRequest("GET", address, nil)
		res, err = client.Do(req)

		if err != nil {
			log.Print(err)
		}

		err = json.NewDecoder(res.Body).Decode(&albums)

		if err != nil {
			log.Print(err)
		}

		//Requesting all songs from an album
		for j := 0; j < len(albums.Album); j++ {

			var tempAlbum albumResponse

			//Converting album to albumResponse
			tempAlbum.StrAlbum = albums.Album[j].StrAlbum
			tempAlbum.IntYearReleased = albums.Album[j].IntYearReleased
			tempAlbum.StrStyle = albums.Album[j].StrStyle
			tempAlbum.StrGenre = albums.Album[j].StrGenre
			tempAlbum.StrLabel = albums.Album[j].StrLabel
			tempAlbum.StrReleaseFormat = albums.Album[j].StrReleaseFormat

			address = bandsURL + "/track.php?"
			address = addParam("m", albums.Album[j].IDAlbum, address)

			req, _ := http.NewRequest("GET", address, nil)
			res, err := client.Do(req)
			if err != nil {
				log.Print(err)
			}

			err = json.NewDecoder(res.Body).Decode(&tracks)

			if err != nil {
				log.Print(err)
			}

			//	Converting tracks
			for k := 0; k < len(tracks.Track); k++ {

				tempTrack.StrTrack = tracks.Track[k].StrTrack

				tempDuration, _ := strconv.Atoi(tracks.Track[k].IntDuration)
				tempDuration = tempDuration / 1000
				tempTrack.StrDuration = strconv.Itoa(tempDuration/60) + ":"
				tempDuration = tempDuration % 60
				if tempDuration < 10 {
					tempTrack.StrDuration = tempTrack.StrDuration + "0" + strconv.Itoa(tempDuration)
				} else {
					tempTrack.StrDuration = tempTrack.StrDuration + strconv.Itoa(tempDuration)
				}


				tempAlbum.Tracks = append(tempAlbum.Tracks, tempTrack)

			}

			messageResponse[i].Albums = append(messageResponse[i].Albums, tempAlbum)

		}

		//	Sorting albums by year
		sort.Slice(messageResponse[i].Albums[:], func(j, k int) bool {
			return messageResponse[i].Albums[j].IntYearReleased < messageResponse[i].Albums[k].IntYearReleased
		})

	}

	var buffer = new(bytes.Buffer)

	err = json.NewEncoder(buffer).Encode(messageResponse)
	if err != nil {
		errorWrapper(w, "Could not encode the struct!", http.StatusInternalServerError)
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	//Streams binary into ResponseWriter

	_, err = io.Copy(w, buffer)
	if err != nil {
		log.Print(err)
	}

}
