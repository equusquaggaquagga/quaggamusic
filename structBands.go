package quaggamusic

// ---------- These are the response structs ----------
// ---------- They have no ID's and include album and track arrays ----------

const bandsURL = "https://theaudiodb.com/api/v1/json/195236"

type artistResponse struct {
	StrArtist      string `json:"strArtist"`
	IntFormedYear  string `json:"intFormedYear"`
	IntBornYear    string `json:"intBornYear"`
	IntDiedYear    string `json:"intDeadYear"`
	BooDisbanded   bool   `json:"booDisbanded"`
	StrStyle       string `json:"strStyle"`
	StrGenre       string `json:"strGenre"`
	StrMood        string `json:"strMood"`
	StrBiographyEN string `json:"strBiographyEN"`
	IntMembers     string `json:"intMembers"`

	Albums []albumResponse `json:"albums"`
}

type albumResponse struct {
	StrAlbum         string `json:"strAlbum"`
	IntYearReleased  string `json:"intYearReleased"`
	StrStyle         string `json:"strStyle"`
	StrGenre         string `json:"strGenre"`
	StrLabel         string `json:"strLabel"`
	StrReleaseFormat string `json:"strReleaseFormat"`

	Tracks []trackResponse `json:"tracks"`
}

type trackResponse struct {
	StrTrack    string `json:"strTrack"`
	StrDuration string `json:"strDuration"`
}

// ---------- These are the received structs ----------

type receivedArtists struct {
	Artists []artist `json:"artists"`
}

type artist struct {
	StrArtist      string `json:"strArtist"`
	IDArtist       string `json:"idArtist"`
	IntFormedYear  string `json:"intFormedYear"`
	IntBornYear    string `json:"intBornYear"`
	IntDiedYear    string `json:"intDeadYear"`
	StrDisbanded   string `json:"strDisbanded"`
	StrStyle       string `json:"strStyle"`
	StrGenre       string `json:"strGenre"`
	StrMood        string `json:"strMood"`
	StrBiographyEN string `json:"strBiographyEN"`
	IntMembers     string `json:"intMembers"`
}

type receivedAlbums struct {
	Album []album `json:"album"`
}

type album struct {
	IDAlbum          string `json:"idAlbum"`
	StrAlbum         string `json:"strAlbum"`
	IntYearReleased  string `json:"intYearReleased"`
	StrStyle         string `json:"strStyle"`
	StrGenre         string `json:"strGenre"`
	StrLabel         string `json:"strLabel"`
	StrReleaseFormat string `json:"strReleaseFormat"`
}

type receivedTracks struct {
	Track []track `json:"track"`
}

type track struct {
	StrTrack    string `json:"strTrack"`
	IntDuration string `json:"intDuration"`
}
