package quaggamusic

import (
	"log"
)

func discordEvent(s string) string {

	address := eventURL //	Sets base url

	//  Adding paramters to url
	address = addParam("keyword", s, address)
	address = addParam("size", "5", address)

	response, err := getResponse(address) // Gets response from address

	if err != nil {
		log.Print(err)
		return "Unable to find events using keyword " + s
	}

	if len(response.EventResponse) == 0 {
		return "Unable to find events using keyword " + s
	}

	// Formats fetched data in a string
	events := "Events:\n"
	for i := 0; i < len(response.EventResponse); i++ {
		events += "\tName: " + response.EventResponse[i].Name + "\n"
		events += "\tEvent Date Time: " + response.EventResponse[i].EventDateTime + "\n"
		events += "\tLink: " + response.EventResponse[i].Link + "\n"
		for j := 0; j < len(response.EventResponse[i].VenuesResponse); j++ {
			events += "\t" + response.EventResponse[i].VenuesResponse[j].Address + "\n"
			events += "\t" + response.EventResponse[i].VenuesResponse[j].City + "\n"
			events += "\t" + response.EventResponse[i].VenuesResponse[j].State + "\n"
			events += "\t" + response.EventResponse[i].VenuesResponse[j].Country + "\n\n"
		}
	}

	return events
}
