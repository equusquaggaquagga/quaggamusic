package quaggamusic

// ---------- These are the response structs ----------
const eventURL = "https://app.ticketmaster.com/discovery/v2/events.json?segmentId=KZFzniwnSyZfZ7v7nJ&apikey=WK7041MZi6mNjYvqn3aemHLPsujlKzz1&"

type response struct {
	EventResponse []eventResponse `json:"events"`
	TotalPages    int             `json:"totalPages"`
}

type eventResponse struct {
	Name           string          `json:"name"`
	VenuesResponse []venueResponse `json:"venues"`
	EventDateTime  string          `json:"eventDateTime"`
	Tickets        tickets         `json:"tickets"`
	Link           string          `json:"link"`
}

type venueResponse struct {
	Address string `json:"address"`
	City    string `json:"city"`
	State   string `json:"state"`
	Country string `json:"country"`
}

type tickets struct {
	SaleStart string `json:"saleStart"`
	SaleEnd   string `json:"saleEnd"`
	Status    string `json:"status"`
}

// ---------- These are the received structs ----------
// ---------- They follow the format of Ticketmaster API ----------

type eventsReceived struct {
	Embedded embedded `json:"_embedded"`
	Page     page     `json:"page"`
}

type embedded struct {
	Events []event `json:"events"`
}

type event struct {
	Name            string           `json:"name"`
	ID              string           `json:"id"`
	URL             string           `json:"url"`
	Sales           sales            `json:"sales"`
	Dates           dates            `json:"dates"`
	Classifications []classification `json:"classifications"`
	PriceRanges     []priceRange     `json:"priceRanger"`
	InnerEmbedded   innerEmbedded    `json:"_embedded"`
}

type sales struct {
	SalesPublic salesPublic `json:"public"`
}

type salesPublic struct {
	StartDateTime string `json:"startDateTime"`
	EndDateTime   string `json:"endDateTime"`
}

type dates struct {
	StartDate startDate `json:"start"`
	Status    status    `json:"status"`
	Timezone  string    `json:"timezone"`
}

type startDate struct {
	LocalDate string `json:"localDate"`
	LocalTime string `json:"localTime"`
}

type status struct {
	Code string `json:"code"`
}

type classification struct {
	Segment  segment  `json:"segment"`
	Genre    genre    `json:"genre"`
	SubGenre subgenre `json:"subGenre"`
}

type segment struct {
	Name string `json:"name"`
	ID   string `json:"id"`
}

type genre struct {
	Name string `json:"name"`
	ID   string `json:"id"`
}

type subgenre struct {
	Name string `json:"name"`
	ID   string `json:"id"`
}

type priceRange struct {
	Currency string  `json:"currency"`
	Min      float64 `json:"min"`
	Max      float64 `json:"max"`
}

type innerEmbedded struct {
	Venues []venue `json:"venues"`
}

type venue struct {
	Name    string  `json:"name"`
	ID      string  `json:"id"`
	City    city    `json:"city"`
	Country country `json:"country"`
	State   state   `json:"state"`
	Address address `json:"address"`
}

type city struct {
	Name string `json:"name"`
}

type country struct {
	Name string `json:"name"`
}

type state struct {
	Name string `json:"name"`
}

type address struct {
	Line string `json:"line1"`
}

type page struct {
	TotalPages int `json:"totalPages"`
}
