module quaggamusic

go 1.13

require (
	firebase.google.com/go v3.10.0+incompatible
	github.com/bwmarrin/discordgo v0.20.1
	google.golang.org/api v0.14.0
)
