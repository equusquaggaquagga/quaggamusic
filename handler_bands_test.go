package quaggamusic

import (
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestBandsHandler(t *testing.T) {

	req, err := http.NewRequest("GET", "/api/v1/searchBands?name=sabaton", nil)
	if err != nil {
		log.Print(err)
	}

	rw := httptest.NewRecorder()
	handler := http.HandlerFunc(HandlerBands)

	handler.ServeHTTP(rw, req)

	if status := rw.Code; status != http.StatusOK {
		t.Errorf("Got non 200 status code: %d", status)
	}
}

func TestBandsHandlerWithoutParameters(t *testing.T) {

	req, err := http.NewRequest("GET", "/api/v1/searchBands?", nil)
	if err != nil {
		log.Print(err)
	}

	rw := httptest.NewRecorder()
	handler := http.HandlerFunc(HandlerBands)

	handler.ServeHTTP(rw, req)

	if status := rw.Code; status != http.StatusBadRequest {
		t.Errorf("Got non 400 status code: %d", status)
	}
}
