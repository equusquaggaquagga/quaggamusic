package quaggamusic

import (
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestStatusHandler(t *testing.T) {

	req, err := http.NewRequest("GET", "/api/v1/status", nil)
	if err != nil {
		log.Print(err)
	}

	rw := httptest.NewRecorder()
	handler := http.HandlerFunc(HandlerStatus)

	handler.ServeHTTP(rw, req)

	if status := rw.Code; status != http.StatusOK {
		t.Errorf("Got non 200 status code: %d", status)
	}
}
