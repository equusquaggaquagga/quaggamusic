package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"quaggamusic"

	"time"

	"github.com/bwmarrin/discordgo"
)

func main() {

	//Keeps track of time at start
	quaggamusic.Uptime = time.Now()

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
		fmt.Println("$PORT not found! Setting to 8080")
	}

	//Initialising discord bot
	dg, err := discordgo.New("Bot " + quaggamusic.DiscordToken)
	if err != nil {
		log.Println("Error creating Discord session", err)
	}

	dg.AddHandler(quaggamusic.MessageCreate)

	//opening connection to discord
	if err := dg.Open(); err != nil {
		log.Println("Error opening connection for discord bot", err)
	}

	//Updating status of discord bot to show help command
	err = dg.UpdateStatus(1, quaggamusic.Prefix+"help")
	if err != nil {
		log.Println("Error updating status of discord bot")
	}

	http.Handle("/", http.FileServer(http.Dir("./static")))
	http.HandleFunc("/api/v1/searchArtists", quaggamusic.HandlerArtists)
	http.HandleFunc("/api/v1/searchEvents", quaggamusic.HandlerEvents)
	http.HandleFunc("/api/v1/searchBands", quaggamusic.HandlerBands)
	http.HandleFunc("/api/v1/playlist", quaggamusic.HandlerPlaylist)
	http.HandleFunc("/api/v1/playlistDatabase", quaggamusic.HandlerPlaylistDatabase)
	http.HandleFunc("/api/v1/status", quaggamusic.HandlerStatus)

	fmt.Println("Listening on port " + port)

	log.Fatal(http.ListenAndServe(":"+port, nil))

}
