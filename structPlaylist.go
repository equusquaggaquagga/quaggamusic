package quaggamusic

const fMURL = "http://ws.audioscrobbler.com/2.0/?method=artist.gettoptracks&api_key=edfad91dbc7c17ac8a20b8349c35f5b4&format=json"

type responsePlaylist struct {
	SongName   string `json:"songName"`
	ArtistName string `json:"artistName"`
	URL        string `json:"url"`
}

type responsePlaylistWithID struct {
	ID string `json:"id"`
	ResponsePlaylist []responsePlaylist `json:"playlist"`
}

// ---------- Payload  struct ----------

type artistsPayload struct {
	Artists []string
}

// ---------- LastFM struct ----------

type topTracksFM struct {
	TopTracks tracksFM `json:"toptracks"`
}

type tracksFM struct {
	Tracks []trackFM `json:"track"`
}

type trackFM struct {
	Name string `json:"name"`
	URL  string `json:"url"`
}

type databasePayload struct {
	SongArtists string `json:"songArtists"`
	SongTitles  string `json:"songTitles"`
	VideoURLs   string `json:"videoUrls"`
}

type databasePlaylist struct {
	ID          string `json:"ID"`
	SongArtists string `json:"songArtists"`
	SongTitles  string `json:"songTitles"`
	VideoURLs   string `json:"videoUrls"`
}
