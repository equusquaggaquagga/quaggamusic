package quaggamusic

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"strings"
)

//HandlerArtists is the handler function called whenever "searchSong" endpoint is used
func HandlerArtists(w http.ResponseWriter, r *http.Request) {
	var discogsData releaseTitleResults
	var deliveryStruct artistDelivStruct
	t := releaseTitleResults{}

	payload := payloadArtists{}                     // Declaring an empty payload struct for decoding
	err := json.NewDecoder(r.Body).Decode(&payload) // Decoding payload into struct

	if err != nil {
		errorWrapper(w, "Unprocessable payload", http.StatusBadRequest)
		log.Println(err)
		return
	}
	if payload.Song == "" { // Song name is needed to continue endpoint
		errorWrapper(w, "Missing song name", http.StatusBadRequest)
		log.Println(err)
		return
	}

	var res *http.Response
	getURL := searchForSongName + payload.Song + "&token=" + discogsAuth // creates URL

	notOnLastPage := true
	page := 1
	for notOnLastPage {
		var temp2 releaseTitleResults
		res, err = http.Get(getURL) // Get requested content
		if err != nil {
			errorWrapper(w, "Could not get content", http.StatusNotFound)
			log.Println(err)
			return
		}
		defer res.Body.Close()
		err = json.NewDecoder(res.Body).Decode(&temp2) //Decode Results
		if err != nil {
			errorWrapper(w, "Unable to decode", http.StatusInternalServerError)
			return
		}
		for i := 0; i < len(temp2.Results); i++ { // For each result
			if temp2.Results[i].Year != "" { // If it has a year (this is because the api returns artists as well as songs
				t.Results = append(t.Results, temp2.Results[i]) //  with the query results, to remove artists we check if they have a year)
			}
		}

		page++
		if page > discogsData.Pagination.Pages { // If past the last page
			notOnLastPage = false
		} else {
			getURL = discogsData.Pagination.Urls.Next // Otherwise get the url for the next page
		}

	}

	if payload.Year != "" { // If year is specified
		t = filterYear(&t, payload.Year) // Call filter function
		if len(t.Results) == 0 {         // If returned empty
			er := "There are no songs with name: " + payload.Song + " and year: " + payload.Year
			errorWrapper(w, er, http.StatusNotFound)
			return
		}
	}

	if payload.Genre != "" { // If genre is specified
		t = filterGenre(&t, payload.Genre) // Call filter function
		if len(t.Results) == 0 {           // If returned empty
			er := "There are no songs with name: " + payload.Song + " and genre: " + payload.Genre
			errorWrapper(w, er, http.StatusNotFound)
			return
		}
	}

	// For each entry still in t, get put together the necessary info for the delivery struct
	for i := 0; i < len(t.Results); i++ {
		var temp artistData
		names := strings.Split(t.Results[i].Title, "-") // Splits string to get bandname and songname
		temp.BandName = names[0]
		temp.SongName = names[1][1:] // Removes emty space at start of string

		rmvspace := strings.Split(t.Results[i].Title, " ")    // Splits string by empty spaces to use external
		temp.Link = youtubeSearchURL + replaceSpace(rmvspace) // function to replace them with +

		temp.Year = t.Results[i].Year

		for j := 0; j < len(t.Results[i].Genre); j++ { // Adds genres to temp
			temp.Genre = append(temp.Genre, t.Results[i].Genre[j])
		}

		deliveryStruct.ArtistData = append(deliveryStruct.ArtistData, temp)
	}
	deliveryStruct.Searchstring = payload.Song

	if len(deliveryStruct.ArtistData) == 0 { // If array is empty
		er := "There are no songs with songname: " + payload.Song
		errorWrapper(w, er, http.StatusNotFound)
		return
	}

	var buffer = new(bytes.Buffer)

	err = json.NewEncoder(buffer).Encode(deliveryStruct)
	if err != nil {
		errorWrapper(w, "Unable to encode struct", http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json") // Displays the mesage in json formating
	w.WriteHeader(http.StatusOK)

	//Streams binary into ResponseWriter
	_, err = io.Copy(w, buffer)
	if err != nil { // If array is empty
		errorWrapper(w, "Unable to send data to server", http.StatusInternalServerError)
		return
	}
}

// Takes releaseTitleResults struct and a string. looks for a match between the string and the content of the struct
func filterYear(res *releaseTitleResults, year string) releaseTitleResults {
	var temp releaseTitleResults
	for i := 0; i < len(res.Results); i++ { // For each entry in slice
		if res.Results[i].Year == year { // If year matches
			temp.Results = append(temp.Results, res.Results[i])
		}
	}
	return temp
}

// Takes releaseTitleResults struct and a string. looks for a match between the string and the content of the struct
func filterGenre(res *releaseTitleResults, genre string) releaseTitleResults {
	var temp releaseTitleResults
	for i := 0; i < len(res.Results); i++ { // For each entry in slice
		for j := 0; j < len(res.Results[i].Genre); j++ { // For each genre in slice
			if res.Results[i].Genre[j] == genre { // If genre matches
				temp.Results = append(temp.Results, res.Results[i])
			}
		}
	}
	return temp
}

//    This function replaces spaces in a string slice with "_"
func replaceSpace(s []string) string {
	var retString string //    String to be returned

	i := 0

	if len(s) >= 2 { // Checks if name is more than one word
		for i < len(s) { // Loops through slice and adds index
			retString += s[i]

			if i != len(s)-1 { // If current index isn't last, "_" is appended
				retString += "+"
			}

			i++ // Counts up
		}
	} else { // If name is 1 word, name is set
		retString = s[0]
	}

	return retString // Returns name with "_" instead of spaces or 1 word name
}
