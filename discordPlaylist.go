package quaggamusic

import "log"

func discordPlaylist(names []string) string {

	//	Declaring variables for later
	var playlistMessage []responsePlaylist
	var payload artistsPayload

	//	Loops through all the artist names from user and appends them to payload
	for i := 0; i < len(names); i++ {
		payload.Artists = append(payload.Artists, names[i])
	}

	//	Gets a random playlist from getPlaylist() in handlerPlaylist.go
	playlistMessage, err := getPlaylist(payload, "10")
	if err != nil {
		log.Print(err)
		return "Unable to find playlist"
	}

	//	Will let user know if no playlist was found
	if len(playlistMessage) == 0 {
		return "Unable to find playlist"
	}

	//	Puts formatted playlist info in string
	playlist := "Playlist:\n"

	for i := 0; i < len(playlistMessage); i++ {
		playlist += "\tSongname: " + playlistMessage[i].SongName + "\n"
		playlist += "\tBandname: " + playlistMessage[i].ArtistName + "\n"
		playlist += "\tURL: " + playlistMessage[i].URL + "\n\n"
	}

	return playlist
}
