package quaggamusic

import (
	"log"
	"strings"

	"github.com/bwmarrin/discordgo"
)

//Prefix contains the prefix needed to use commands for discord bot
const Prefix string = "?"

//MessageCreate will be called everytime a new message is sent in a channel the bot has access to
func MessageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

	//Prevents bot from using it's own commands
	if m.Author.ID == s.State.User.ID {
		return
	}

	cmd := strings.Split(m.Content, " ") // Splits command with each space

	//	Switch for checking which command was used
	switch cmd[0] {

	//	Ping is a basic command which only replies with "Pong!
	case Prefix + "ping":
		_, err := s.ChannelMessageSend(m.ChannelID, "Pong!")
		if err != nil {
			log.Println("Error when using ping command")
		}

	//	Event fetches all events based on keyword used
	case Prefix + "event":
		if len(cmd) == 2 { // Event will only work with one key word, sends error if not used correctly
			_, err := s.ChannelMessageSend(m.ChannelID, discordEvent(cmd[1]))
			if err != nil {
				log.Println("Error when using event command")
			}
		} else {
			_, err := s.ChannelMessageSend(m.ChannelID, "Not correct amount of arguments in command!")
			if err != nil {
				log.Println("Error when sending event wrong command usage")
			}
		}

	// Playlist fetches a random playlist based on artist names used with command
	case Prefix + "playlist":
		if len(cmd) >= 2 { // Playlist will only work if at least 1 name is used
			_, err := s.ChannelMessageSend(m.ChannelID, discordPlaylist(cmd))
			if err != nil {
				log.Println("Error when using playlist command")
			}
		} else {
			_, err := s.ChannelMessageSend(m.ChannelID, "Not enough band names")
			if err != nil {
				log.Println("Error when sending playlist wrong command usage")
			}
		}

	// Help displays all other commands and how to use them
	case Prefix + "help":
		_, err := s.ChannelMessageSend(m.ChannelID, "Commands:\n\t?ping\n\t?event <keyword>\n\tplaylist <artistname1>")
		if err != nil {
			log.Println("Error when using help command")
		}
	default:
		return
	}
}
