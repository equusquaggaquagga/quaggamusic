package quaggamusic

import (
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestEventsHandler(t *testing.T) {

	req, err := http.NewRequest("GET", "/api/v1/searchEvents?city=Oslo&country=NO&startDate=2019-12-12", nil)
	if err != nil {
		log.Print(err)
	}

	rw := httptest.NewRecorder()
	handler := http.HandlerFunc(HandlerEvents)

	handler.ServeHTTP(rw, req)

	if status := rw.Code; status != http.StatusOK {
		t.Errorf("Got non 200 status code: %d", status)
	}
}

func TestEventsHandlerWithoutParameters(t *testing.T) {

	req, err := http.NewRequest("GET", "/api/v1/searchEvents?", nil)
	if err != nil {
		log.Print(err)
	}

	rw := httptest.NewRecorder()
	handler := http.HandlerFunc(HandlerEvents)

	handler.ServeHTTP(rw, req)

	if status := rw.Code; status != http.StatusBadRequest {
		t.Errorf("Got non 400 status code: %d", status)
	}
}
