package quaggamusic

import (
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestPlaylistDatabaseHandler(t *testing.T) {

  log.Print("Testing without parameters")

	req, err := http.NewRequest("GET", "/api/v1/playlistDatabase", nil)
	if err != nil {
		log.Print(err)
	}

	rw := httptest.NewRecorder()
	handler := http.HandlerFunc(HandlerPlaylistDatabase)

	handler.ServeHTTP(rw, req)

	if status := rw.Code; status != http.StatusOK {
		t.Errorf("Got non 200 status code: %d", status)
	}
}

func TestPlaylistDatabaseHandlerWithID(t *testing.T) {

  log.Print("Testing with parameters")

	req, err := http.NewRequest("GET", "/api/v1/playlistDatabase?id=5zMnsOW213W9MBkkUCrt", nil)
	if err != nil {
		log.Print(err)
	}

	rw := httptest.NewRecorder()
	handler := http.HandlerFunc(HandlerPlaylistDatabase)

	handler.ServeHTTP(rw, req)

	if status := rw.Code; status != http.StatusOK {
		t.Errorf("Got non 200 status code: %d", status)
	}
}

func TestPlaylistDatabaseHandlerWithIncorrectID(t *testing.T) {

  log.Print("Testing with incorrect parameters")

	req, err := http.NewRequest("GET", "/api/v1/playlistDatabase?id=huj", nil)
	if err != nil {
		log.Print(err)
	}

	rw := httptest.NewRecorder()
	handler := http.HandlerFunc(HandlerPlaylistDatabase)

	handler.ServeHTTP(rw, req)

	if status := rw.Code; status != http.StatusNotFound {
		t.Errorf("Got non 404 status code: %d", status)
	}
}
